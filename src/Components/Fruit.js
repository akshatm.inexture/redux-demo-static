import React from "react";
import appleImage from "../Assets/Images/apple.jpeg";
import "../Assets/CSS/Fruit.css";


function Fruit(props) {
  return (
    <>
      <div className="fruitDetail">
        <h1 className="fruitHead">Fruit Details</h1>
        <div className="appleImage">
          <img src={appleImage} />
        </div>
        <div className="text">
          <span style={{textDecoration:'underline'}}>Name:</span>
          <span>Apple</span>
        </div>

        <div className="text-Para">
          <span style={{textDecoration:'underline'}}>Description:---</span>
          <span>
            Apples grown from seed tend to be very different from those of their
            parents, and the resultant fruit frequently lacks desired
            characteristics. Generally, apple cultivars are propagated by clonal
            grafting onto rootstocks. Apple trees grown without rootstocks tend
            to be larger and much slower to fruit after planting. Rootstocks are
            used to control the speed of growth and the size of the resulting
            tree, allowing for easier harvesting.
          </span>
        </div>
        <div className="btn-grp">
          <button className="btn" onClick={
                        ()=>{props.addToCartHandler({Name:'Apple',Description:'Apples grown from seed tend to be very different from those of parents, and the resultant fruit frequently lacks characteristics. Generally, apple cultivars are propagated by  grafting onto rootstocks. Apple trees grown without rootstocks tens to be larger and much slower to fruit after planting. Rootstocks are  used to control the speed of growth and the size of the resulting tree, allowing for easier harvesting.'})}
                        }>Add to Cart</button>
          <button className="btn"  onClick={
                        ()=>{props.removeToCartHandler()}
                        }>Remove to Cart</button>
        </div>
      </div>
    </>
  );
}

export default Fruit;
