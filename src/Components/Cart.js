import React from 'react'
import '../Assets/CSS/Fruit.css'
function Cart(props) {
    console.log(props.data)
  return (
    <>
    <div style={{textAlign:'end',margin:'1rem'}}>
        <span className='cart-count'>{props.data.length}</span>
    <i className="fa fa-shopping-cart fa-5x"  aria-hidden="true"></i>
    </div>
    </>
  )
}

export default Cart