import logo from './logo.svg';
import './App.css';
import Fruit from './Components/Fruit';
import Cart from './Components/Cart';
import CartContainer from './Containers/CartContainer';
import FruitContainer from './Containers/FruitContainer';


function App() {
  return (
    <>
      <CartContainer/>
      <FruitContainer/>
    
    </>
 
  );
}

export default App;
